import "dotenv/config";

export default {
  client: "pg",
  connection: {
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    user: process.env.DB_USERNAME,
  },
  pool: { min: 0, max: 10 },
  migrations: {
    directory: "src/database/migrations",
    extension: "js",
  },
};
