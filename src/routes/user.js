import express from "express";
import bodyParser from "body-parser";
import { userController } from "../controllers/users.js";
import { authController } from "../controllers/auth.js";

const router = express.Router();

router.use(bodyParser.json());

router.post("/", async (req, res) => {
  await userController.create(req.body, res);
});

router.post("/signIn", async (req, res) => {
  const { login, password } = req.body;
  await authController.signIn(login, password, res);
});

export default router;
