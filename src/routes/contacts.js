import express from "express";
import { contactsController } from "../controllers/contacts.js";
import bodyParser from "body-parser";
import { verifyToken } from "../middleware/auth.js";

const router = express.Router();

router.use(bodyParser.json());

router.get("/", verifyToken, async (req, res) => {
  await contactsController.getAll(req.user, res);
});

router.post("/", verifyToken, async (req, res) => {
  await contactsController.insert(req.user, req.body.contacts, res);
});

export default router;
