import swaggerJsdoc from "swagger-jsdoc";

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Suflex Products API",
      description: "A simple API created for evaluation",
      contact: {
        name: "Igor Rhein",
      },
      servers: ["http://localhost:8000"],
    },
    schemes: ["http"],
    consumes: ["application/json"],
    produces: ["application/json"],
    securityDefinitions: {
      bearerAuth: {
        type: "apiKey",
        in: "header",
        name: "token",
      },
    },
  },
  apis: ["./src/swagger/routes.js"],
};

export const swaggerDocs = swaggerJsdoc(swaggerOptions);
