/**
 * @swagger
 * /contacts:
 *  get:
 *    tags:
 *    - Contacts
 *    security:
 *    - bearerAuth: []
 *    description: Fetches all registered contacts
 *    responses:
 *      '200':
 *        description: Successfully fetched all contacts
 *        schema:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: integer
 *              name:
 *                type: string
 *              number:
 *                type: string
 *      '400':
 *        description: Failed to fetch products
 *
 *  post:
 *    tags:
 *    - Contacts
 *    security:
 *    - bearerAuth: []
 *    description: Registers contacts of a valid user
 *    parameters:
 *    - in: body
 *      name: body
 *      schema:
 *        type: object
 *        properties:
 *          contacts:
 *            type: array
 *            items:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                number:
 *                  type: string
 *    responses:
 *      '200':
 *        description: Successfully fetched contacts
 *        schema:
 *          type: object
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: integer
 *              name:
 *                type: string
 *              number:
 *                type: string
 *      '400':
 *        description: Failed to fetch products
 *
 *
 * /user:
 *  post:
 *    tags:
 *    - User
 *    description: Registers a new user
 *    parameters:
 *    - in: body
 *      name: body
 *      schema:
 *        type: object
 *        properties:
 *          login:
 *            type: string
 *          password:
 *            type: string
 *          name:
 *            type: string
 *
 *    responses:
 *      '200':
 *        description: Successfully created user
 *        schema:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: integer
 *              name:
 *                type: string
 *              login:
 *                type: string
 *              bearerToken:
 *                type: string
 *      '400':
 *        description: Failed to fetch products
 *
 * /user/signIn:
 *  post:
 *    tags:
 *    - User
 *    description: Registers a new user
 *    parameters:
 *    - in: body
 *      name: body
 *      schema:
 *        type: object
 *        properties:
 *          login:
 *            type: string
 *          password:
 *            type: string
 *
 *    responses:
 *      '200':
 *        description: Successfully created user
 *        schema:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: integer
 *              name:
 *                type: string
 *              login:
 *                type: string
 *      '400':
 *        description: Failed to fetch products
 */
