export async function up(knex) {
  await knex.schema.createTable("contacts", (table) => {
    table.increments("id").primary();
    table.string("name", 100).notNull();
    table.integer("phone", 13).notNull();
    table.integer("user_id").notNull().references("id").inTable("users");
  });
}

export async function down(knex) {
  await knex.schema.dropTableIfExists("contacts");
}
