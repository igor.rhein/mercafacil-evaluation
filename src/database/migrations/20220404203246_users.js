export async function up(knex) {
  await knex.schema.createTable("users", (table) => {
    table.increments("id").primary();
    table.string("name").notNull();
    table.integer("login").notNull();
    table.integer("password").notNull();
  });
}

export async function down(knex) {
  await knex.schema.dropTableIfExists("users");
}
