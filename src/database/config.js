import * as Knex from "knex";
import config from "../../knexfile.js";

function getKnexInstance() {
  const conection = Knex.default(config);
  conection
    .raw("SELECT 1")
    .then(() => {
      console.log("PostgreSQL connected");
    })
    .catch((error) => {
      console.log("PostgreSQL not connected");
      console.error(error);
      // throw e;
    });
  return conection;
}

export const knex = getKnexInstance();
