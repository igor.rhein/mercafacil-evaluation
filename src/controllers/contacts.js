import { baseRepository } from "../repository/index.js";

class ContactsController {
  async getAll(user, res) {
    user.id = 1;
    try {
      const contacts =
        (await baseRepository.find("contacts", {
          user_id: user.id,
        })) ?? [];
      const sortedContacts = this.sortItems(contacts);
      res.send(sortedContacts);
    } catch (error) {
      console.log(error);
      res.status(400).send({ message: "Erro ao buscar contatos!" });
    }
  }

  async insert(user, data, res) {
    user.id = 1;
    try {
      await Promise.all(
        data.map(async (item) => {
          const contact = await baseRepository.get("contacts", {
            name: item.name,
            user_id: user.id,
          });
          if (!contact || item.number !== contact.number) {
            await baseRepository.insert("contacts", {
              ...item,
              user_id: user.id,
            });
          }
        })
      );
      return this.getAll(user, res);
    } catch (error) {
      console.log(error);
      res.status(400).send({ message: "Erro ao salvar contatos!" });
    }
  }

  sortItems(items) {
    // console.log("=>", items);
    return items.sort((next, curr) => next.name.localeCompare(curr.name));
  }
}

export const contactsController = new ContactsController();
