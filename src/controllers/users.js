import { baseRepository } from "../repository/index.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import "dotenv/config";

class UserController {
  async create(data, res) {
    const exists = await baseRepository.getUser(data.login);
    if (exists) {
      return res.status(400).send("User already exists!");
    }

    try {
      const hashedPass = await bcrypt.hash(data.password, 10);
      const newUser = {
        name: data.name,
        login: data.login,
        password: hashedPass,
      };
      const user = await baseRepository.insert("users", newUser);
      user.bearerToken = jwt.sign(
        JSON.stringify(user),
        process.env.TOKEN_SECRET
      );

      res.send({
        message: "Usuário criado com sucesso!",
        user,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  }
}

export const userController = new UserController();
