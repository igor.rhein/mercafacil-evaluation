import { baseRepository } from "../repository/index.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import "dotenv/config";

class AuthController {
  async signIn(login, password, res) {
    const user = await baseRepository.getUser(login);
    if (!user) {
      return res.status(404).send("User does not exist!");
    }

    if (await bcrypt.compare(password, user.password)) {
      const bearerToken = jwt.sign(
        JSON.stringify(user),
        process.env.TOKEN_SECRET
      );
      res.send({ bearerToken });
    } else {
      res.status(400).send("Invalid Credentials!");
    }
  }
}

export const authController = new AuthController();
