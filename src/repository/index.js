import { knex } from "../database/config.js";

class BaseRepository {
  constructor(knex) {
    this.knex = knex;
  }

  async find(table, condition) {
    return this.knex(`${table}`)
      .where({ ...condition })
      .select();
  }

  async get(table, condition) {
    return this.knex(`${table}`)
      .where({ ...condition })
      .first();
  }

  async getUser(login) {
    return this.knex(`${table}`).where({ login }).first();
  }

  async insert(table, data) {
    return this.knex(`${table}`).insert(data).returning("*");
  }
}

export const baseRepository = new BaseRepository(knex);
