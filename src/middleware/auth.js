import "dotenv/config";
import jwt from "jsonwebtoken";

export const verifyToken = (req, res, next) => {
  const { token } = req.headers;
  if (!token) {
    return res.status(400).send("Missing authentication!");
  }
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (err) {
    return res.status(400).send("Invalid token!");
  }
};
