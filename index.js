import "dotenv/config";
import express from "express";
import contacts from "./src/routes/contacts.js";
import users from "./src/routes/user.js";
import swaggerUiExpress from "swagger-ui-express";
import { swaggerDocs } from "./src/swagger/index.js";
import { knex } from "./src/database/config.js";
import cors from "cors";

const app = express();
const port = process.env.PORT || 3000;

try {
  app.use(cors());

  // await knex.migrate.latest();

  app.use("/contacts", contacts);

  app.use("/user", users);

  app.use("/docs", swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocs));

  app.listen(port, () => console.log(`Server listening on port ${port}!`));
} catch (error) {
  console.log(["Error while trying to start server!", error]);
}

export default app;
